# Duke-nus.edu.sg Web Accessibility Assessment

__<https://www.duke-nus.edu.sg/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).


Overall its great work but some important issues need remediated for compliance.  Most of it is easy to fix. Some of it will require web developer help and some of it might be simple content related changes.

The assessment is not just a list of things to fix. It also provides brief descriptions and suggested solutions. You may use the suggested solutions, or come up with your own solutions as long as they are compliant. 



## Background and foreground colors do not have a sufficient contrast ratio. [WCAG 1.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#contrast-minimum)

Low-contrast text is difficult or impossible for many users to read. [Learn more](https://web.dev/color-contrast/).

### Most link elements have low contrast.

__Visual location examples:__

![Text with low contrast](assets/Annotation2020-06-30154713b.png) 

from https://www.duke-nus.edu.sg/admissions/why-duke-nus

![Text with low contrast](assets/duke-nus-edu-sg-a-href-tel-6565167666.png) 

from https://www.duke-nus.edu.sg/about/about-duke-nus

![Text with low contrast](assets/Annotation2020-06-30154713.png) 

![Text with low contrast](assets/duke-nus-edu-sg-btn-ct.png)

from https://www.duke-nus.edu.sg/



Element has insufficient color contrast of 2.62 (foreground color: #ff7900, background color: #ffffff, font size: 10.5pt, font weight: normal). Expected contrast ratio of 4.5:1.

#### Suggested solution:

Change all instances of orange #FF7900 as follows:

- Large or bold orange text to 3:1 ratio suggestion [#F46C0F example](https://contrast-finder.tanaguru.com/result.html?foreground=%23FF7900&background=%23FFF&ratio=3&isBackgroundTested=false&algo=Rgb&distanceSort=asc#F46C0F) or other compliant color.
- Small orange text to 4.5:1 ratio suggestion [#C75300 example](https://contrast-finder.tanaguru.com/result.html?foreground=%23FF7900&background=%23FFF&ratio=4.5&isBackgroundTested=false&algo=Rgb&distanceSort=asc) or other compliant color.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,0,FORM,14,DIV,1,FOOTER,0,DIV,0,DIV,0,DIV,1,DIV,1,DIV,1,SPAN,0,A`<br>
Selector:<br>
`a[href="tel\:6565167666"]`
</details>


---

### Other examples of low contrast elements.

The element _"COVID-19 updates: new and current students"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/duke-nus-edu-sg-container-.png)


__HTML location:__

```html
<a href="/admissions/why-duke-nus/faq/covid19-updates">COVID-19 updates: new and current students</a>
```

![Text with low contrast](assets/duke-nus-edu-sg-btn-ct.png)

#### Suggested solution:

Element has insufficient color contrast of 2.37 (foreground color: #ff7900, background color: #fff3cd, font size: 12.0pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,0,FORM,12,HEADER,10,DIV,0,DIV,0,DIV,2,A`<br>
Selector:<br>
`.container > a`
</details>



---



## Inaccurate alt image descriptions [WCAG 1.1.1](https://www.w3.org/WAI/tutorials/images/) [HIGH PRIORITY] 

Screen reader users rely on image alt descriptions to understand content.  Also a common problem that generates complaints.

- There is a large volume of inaccessible images and they have important content in them.  
- This is a major barrier to screen reader users.
- This will generate complaints. Lets fix this before it causes a problem and generates official complaints.

Side note: Images cannot be auto-translated. So Google Translate won't work on some of the most important content on this page. This site might attract international visitors. This is a quasi accessibility issue, but something to consider when using images of text. In addition to their WCAG problems.

__Needs work.__

### Example 1 of infographic with inaccurate alt text.

<https://www.duke-nus.edu.sg/education/about-education/vice-dean's-message>

__Visual location:__

![bad alt text](https://www.duke-nus.edu.sg/images/default-source/education/duke-nus-infographics-(small).png?sfvrsn=83f49370_0)

__HTML location:__

```html
<img id="MainContent_C002_ctl00_ctl00_imageItem" title="Duke-NUS Infographics 2019" src="/images/default-source/education/duke-nus-infographics-(small).png?sfvrsn=83f49370_0" alt="Duke-NUS Infographics 2019">
```


#### Suggested solution:

Provide an equally effective text alterative.

Add all the text below the image and hide it from everyone except screen reader users.  Or [More details on complex image description options here](https://www.w3.org/WAI/tutorials/images/complex/)

If the content creator has the ability to view the HTML source, _and knows HTML_ they could potentially add it themselves.  

1. Transpose the image text in the CMS content editor.
2. Change `alt` text to "READ DESCRIPTION BELOW" so they know it is there.
3. In the source code, wrap it in a `<div>` with the class`.sr-only`.

See example below.

__HTML:__

```html

<img id="MainContent_C002_ctl00_ctl00_imageItem" title="Duke-NUS Infographics 2019" src="/images/default-source/education/duke-nus-infographics-(small).png?sfvrsn=83f49370_0" alt="READ DESCRIPTION BELOW">

<div class="sr-only">
  <h2>Academic Background</h2>
  <ul>
    <li>17% Engineering</li>
    <li>75% Sciences</li>
    <li>5% Arts</li>
    <li>3% Business and others</li>
  <ul> 
  <p> 26.3% of Duke-NUS MD students have completed or been accepted to additional graduate degree programs.</p>
  <h2>Demographics</h2>
  <p>53% Female. 47% Male.</p>
  <p>63% Singaporean. 30% International. 7% Singapore RP.</p>
  <h2>Top Residency Specialties<h2>
  <ul>
    <li>Internal Medicine</li>
    and so on...
  </ul>
  ...
</div>
```

I would expect that it would still be visible in the content editor, but would hide it as expected on the front-end that site visitors see.  However some CMS content editors do not allow custom classes. In that case it would require a web developers help.

Some developers have added a custom field that allows people to enter the equivalent content. This prevents accidental HTML problems that may be introduced by using the CMS content editor.

[More details on image captions and other options here]([WCAG](https://www.w3.org/WAI/tutorials/images/) )

---

### Example 2 of infographic with inaccurate alt text.

https://www.duke-nus.edu.sg/education/research-and-innovations/innovations-in-tel

![bad alt text](https://www.duke-nus.edu.sg/images/default-source/default-album/telfirst.jpg?sfvrsn=fbc1831_0)



#### Suggested solution:

Same as previously mentioned.

---

### Example 3 infographic with innacruate alt text.

https://www.duke-nus.edu.sg/about/about-duke-nus/vision-mission-values

__Visual location:__

![terrible alt text](https://www.duke-nus.edu.sg//images/default-source/default-album/vmv_taglines.jpg?sfvrsn=28c11c16_12)

__HTML location:__
```html
<img id="MainContent_C003_ctl00_ctl00_imageItem" title="vmv_taglines" src="/images/default-source/default-album/vmv_taglines.jpg?sfvrsn=28c11c16_12" alt="">
```

#### Suggested solution:

Same as previously mentioned. However, this specific image is just plain text. Its not really a graphic. The easiest way to deal with it might be to just make it text.

---

### Example 4 of infographic with inaccurate alt text.

https://www.duke-nus.edu.sg/admissions/pre-md-pathways/nus-engineering-and-medicine

__Visual location:__

![another example of the epidemic of bad alt text](https://www.duke-nus.edu.sg//images/default-source/education/nusengineering-medicine-track.png?sfvrsn=f740e2b7_0)

__HTML location:__

```html
<img id="MainContent_C007_ctl00_ctl00_imageItem" title="NUS Engineering &amp;amp;amp; Medicine Track" src="/images/default-source/education/nusengineering-medicine-track.png?sfvrsn=f740e2b7_0" alt="NUS Engineering &amp;amp;amp; Medicine Track">
```

#### Suggested solution:

Same as previously mentioned.


---


## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---



## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Success!__

---





## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__

---






## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).


### Main menu needs ARIA attributes that describe the state of the menu [HIGH PRIORITY]

__Needs work.__

Main menu lacks the behaviors or ARIA attributes necessary for screen reader users to use the menu.  

The most basic implementation would be:
- Add keyboard functionality that allows the enter key or spacebar to toggle the visibility of the dropdown menu. 
- Add `aria-haspopup="true"` to the top level `<a>` trigger elements
- Add `aria-expanded="false"` change it to `true` when it is expanded. 

See "Approach 1: Use parent as toggle" here https://www.w3.org/WAI/tutorials/menus/flyout/ for more info.

#### Suggested solution:

It needs to behave in a similar manner to this https://duke.edu/ menu. Hitting Enter key expands and contracts the dropdown menu.

There are many ways to implement screen reader accessible menu.

Option 1: This website already uses the Bootstrap framework. Version 4 of Bootstrap comes with this functionality.  It appears that adding a few attributes as instructed on this page https://getbootstrap.com/docs/4.0/components/navbar/ will enable the necessary functionality.

Option 2: Create your own custom code that will toggle the visibility of the sub menus.

For further assistance email us at web-accessibility@duke.edu. We probably have some code snippets available that will handle this.



---





## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---





## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Not applicable.__

---





## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success after "Main menu needs ARIA attributes that describe the state of the menu"__

---





## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

__Success!__

---






## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success after "Main menu needs ARIA attributes that describe the state of the menu"__

---





## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---





## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__

---




<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.




